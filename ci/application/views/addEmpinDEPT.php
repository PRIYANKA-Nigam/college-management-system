<?php
include("inc/header.php");
$image =base_url().'assets/office.jpg';
?>
<div class="container">
<?php $college_id = $this->uri->segment(3);
foreach($collegename as $cllg)
echo "<div style='background-color:red;display:block;width:320px;margin-top:20px'><h2 style='color:green;'>$cllg->collegename Department</h2></div>";
?>
    <?php echo form_open("users/createEmpinDEPT/{$college_id}" , ['class'=> 'form-horizontal']);   ?>
<h3 class="display-3" style="text-align: center;">Add Employee</h3><hr><br>

<?php  if($msg= $this->session->flashdata('message')):  ?>
        <div class="alert alert-dismissible alert-success"><?php echo $msg;?></div>
    <?php endif;  ?>
<!-- call to undefined function form_open() is coming to remove this add 'form' inside helper in autoload.php -->
<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Employee name</label>
    <div class="col-md-9">
        <?php echo form_input(['name'=>'studentname','class'=>'form-control','placeholder'=>'Enter name','value'=>set_value('username')]); ?>
    </div>
</div>
	</div>
	<div class="col-md-6">
	<?php echo form_error('studentname','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>

        <?php 
        echo form_hidden('college_id',$college_id);
        ?>
<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Email</label>
    <div class="col-md-9">
        <?php echo form_input(['name'=>'email','class'=>'form-control','placeholder'=>'Enter Email','value'=>set_value('email')]); ?>
    </div>
</div>
	</div>
	<div class="col-md-6">
    <?php echo form_error('email','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>

<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Gender</label>
    <select class="col-lg-9" name="gender">
      <option value="">Select Gender</option>
      <option value="Male">Male</option>
      <option value="Female">Female</option>
      <option value="Transgender">Transgender</option>
    </select>
</div>
	</div>
	<div class="col-md-6">
    <?php echo form_error('gender','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>
<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Grade</label>
    <select class="col-lg-9" name="course">
      <option value="">Select Grade</option>
      <?php if(count($grades)):  ?>
        <?php foreach($grades as $grade):?>
      <option value=<?php echo $grade->grade_type?>><?php echo $grade->grade_type?></option>
       <?php endforeach;?>
      <?php endif;?>
    </select>
</div>
	</div>
	<div class="col-md-6">
	<?php echo form_error('course','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>
<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Band</label>
    <div class="col-md-9">
        <?php echo form_input(['name'=>'band','class'=>'form-control','placeholder'=>'Enter Band(if any)','value'=>set_value('username')]); ?>
    </div>
</div>
	</div>
	<div class="col-md-6">
	<?php echo form_error('band','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>
<button type="submit" class="btn btn-primary">ADD</button>
<?php echo anchor("users/dashboard","BACK" , ['class'=> 'btn btn-primary']);   ?>
</div>
<?php echo form_close(); ?>
