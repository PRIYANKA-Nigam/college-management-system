<?php
include("inc/header.php");
$image =base_url().'assets/office.jpg';
?>
<div class="container">
    <?php echo form_open("admin/modifyStudent/{$studentData->id}" , ['class'=> 'form-horizontal']);   ?>
<h3 class="display-3" style="text-align: center;">Edit Employee</h3><hr><br>
<?php  if($msg= $this->session->flashdata('message')):  ?>
        <div class="alert alert-dismissible alert-success"><?php echo $msg;?></div>
    <?php endif;  ?>
<!-- call to undefined function form_open() is coming to remove this add 'form' inside helper in autoload.php -->
<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Employee name</label>
    <div class="col-md-9">
        <?php echo form_input(['name'=>'studentname','class'=>'form-control','placeholder'=>'Enter name',
        'value'=>set_value('studentname',$studentData->studentname)]); ?>
    </div>
</div>
	</div>
	<div class="col-md-6">
	<?php echo form_error('studentname','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>
<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Email</label>
    <div class="col-md-9">
        <?php echo form_input(['name'=>'email','class'=>'form-control','placeholder'=>'Enter Email',
        'value'=>set_value('email',$studentData->email)]); ?>
    </div>
</div>
	</div>
	<div class="col-md-6">
    <?php echo form_error('email','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>
<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Department Name</label>
    <select class="col-lg-9" name="college_id">
      <!-- <option value="">Select College</option> -->
      <option value="<?php echo $studentData->college_id;  ?>"><?php echo $studentData->collegename;  ?></option>
      <?php if(count($colleges)):  ?>
        <?php foreach($colleges as $college):?>
      <option value=<?php echo $college->college_id?>><?php echo $college->collegename?></option>
       <?php endforeach;?>
      <?php endif;?>
    </select>
</div>
	</div>
	<div class="col-md-6">
    <?php echo form_error('college_id','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>
<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Gender</label>
    <select class="col-lg-9" name="gender">
      <option value="<?php echo $studentData->gender; ?>"><?php echo $studentData->gender; ?></option>
      <option value="Male">Male</option>
      <option value="Female">Female</option>
      <option value="Transgender">Transgender</option>
    </select>
</div>
	</div>
	<div class="col-md-6">
    <?php echo form_error('gender','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>
<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Grade</label>
    <select class="col-lg-9" name="course">
      <option value="">Select Grade</option>
      <?php if(count($grades)):  ?>
        <?php foreach($grades as $grade):?>
      <option value=<?php echo $grade->grade_type?>><?php echo $grade->grade_type?></option>
       <?php endforeach;?>
      <?php endif;?>
    </select>
</div>
	</div>
	<div class="col-md-6">
	<?php echo form_error('course','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>
<div class="row">
	<div class="col-md-6">
<div class="form-group">
    <label class="col-md-3 control-label">Band</label>
    <div class="col-md-9">
        <?php echo form_input(['name'=>'band','class'=>'form-control','placeholder'=>'Enter Band',
                  'value'=>set_value('')]); ?>
    </div>
</div>
	</div>
	<div class="col-md-6">
	<?php echo form_error('band','<div class="text-danger">','</div>');?>
    <img src="<?=$image?>" height="70px" width="550px">
	</div>
</div>
<button type="submit" class="btn btn-primary">EDIT</button>
<?php echo anchor("admin/viewStudents/{$studentData->id}","BACK" , ['class'=> 'btn btn-primary']);   ?>
</div>
<?php echo form_close(); ?>
